#ifndef TIMEOPS_H
#define TIMEOPS_H

#include <time.h>
#include <stdio.h>



int cmp_spec(struct timespec *a, struct timespec *b);
void add_spec_us(struct timespec * s, long time_us);


#endif 
