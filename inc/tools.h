#ifndef PRUDA_TOOLS_H
#define PRUDA_TOOLS_H


#include <time.h>
#include <pthread.h>
#include <semaphore.h>
#include <cstdint>

//#include "task.h"
#include "runqueue.h"
#include "timeops.h"



#include "user.h"

#include "task.h"


#define SINGLE 1
#define MULTIPLE 2
#define MULTIPROC 3


#define EDF 4
#define FP 5








template<typename ...Arguments>
void create_kernel(struct kernel_t<Arguments ...> * k, void kernel_c(Arguments...),  int gs, int bs,
		   Arguments...args);




struct scheduler_t {
  int strategy;
  int policy;

  // Scheduling functionaliteis 
  void (*pruda_subscribe)(struct pruda_task_t* );
  void (*pruda_resched)();
  

  // All pruda tasks queue
  struct pruda_runqueue_t *tq;

  // pruda runqueue 
  struct pruda_runqueue_list_t * rql;


  // Houssam: This part need to be redefined so to dynamically be parametrized  
  // SM runqueues
  struct pruda_runqueue_t *sm0rq;
  struct pruda_runqueue_t *sm1rq;

  // Cuda streams 
    cudaStream_t hsq;
    cudaStream_t lsq;
    int hsq_free;
    int lsq_free;

  struct pruda_task_t * lsq_current;
  struct pruda_task_t * hsq_current;


  // protect resched from multiple accesses
  pthread_mutex_t mut; 



  pthread_barrier_t sync_all_barrier;

  


};

// need to declare indexes methods

void pruda_alloc_sm(int sm);
int pruda_get_sm();
int pruda_check_sm(int sm);
void pruda_thread_exit();
void pruda_kernel_abort();



void submit_task(int indexex);



struct pruda_task_t * create_pruda_task(int id,
					struct gpu_sched_param gpu_params,
					int bs, int gs);

void init_scheduler(int strategy, int policy);


void pruda_subscribe_fp(struct pruda_task_t *);
void pruda_resched_fp();

void pruda_subscribe_edf(struct pruda_task_t *);
void pruda_resched_edf();



void reset_pruda_task_queue();
int add_pruda_task(struct pruda_task_t *);
int del_tail_pruda_task_from_tq();

void sched_on_lsq();
void sched_on_hsq();

void print_pruda_task(const struct pruda_task_t *task);

void *pruda_task(void *args);


void create_cpu_threads();


int pruda_task_queue_size();


#endif 
