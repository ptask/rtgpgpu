#ifndef PRUDA_RUNQUEUE_H
#define PRUDA_RUNQUEUE_H

#define RUNQUEUES_NMB 32
#define MAX_PRUDA_TASK_PER_QUEUE 32


#define SUCCESS 1
#define FAIL 0 


#include <stdlib.h>
#include <stdio.h>

#include "task.h"



struct pruda_runqueue_t{
  struct pruda_task_t * list[MAX_PRUDA_TASK_PER_QUEUE];
  int size; 
};

struct pruda_runqueue_list_t{
  struct pruda_runqueue_t  * list[RUNQUEUES_NMB];
};



// run queue operations
struct pruda_runqueue_t *  create_pruda_runqueue();
int add_tail_pruda_task_runqueue(struct pruda_task_t *, struct pruda_runqueue_t *);
int del_tail_pruda_task_runqueue(struct pruda_runqueue_t *);
struct pruda_task_t * get_tail_pruda_task_runqueue(struct pruda_runqueue_t *);
void destroy_pruda_runqueue(struct pruda_runqueue_t *);

// runqueue list operations
struct pruda_runqueue_list_t * create_pruda_runqueues_list();
void destroy_prudarunqueue_list_t(struct pruda_runqueue_list_t *);


void add_pruda_task_fixed_priority(struct pruda_task_t * tau, struct pruda_runqueue_list_t * rql);
struct pruda_task_t *  get_most_priority_task_fixed_priority(struct pruda_runqueue_list_t * rql);
struct pruda_runqueue_t * get_most_priority_queue_fixed_priority(struct pruda_runqueue_list_t *);



void print_rq_state(struct pruda_runqueue_list_t *rql);
#endif
