#ifndef VAR_DEFS 
#define VAR_DEFS


#include <tuple>
#include <iostream> 

// do not modify
template<typename ...Arguments>
struct kernel_t {
  void (*kernel_c)(Arguments...);
  std::tuple<Arguments...> args;
  int bs;
  int gs;
};

void init_kernel_listing();

// user modifications must be apported here 
std::tuple<struct kernel_t<int *,int*,int *, int> * , struct kernel_t<int *,int*,int *> * >   get_listing();
#endif
