#ifndef TASK_H
#define TASK_H



#include <pthread.h>
#include <semaphore.h>


struct gpu_sched_param {
  long period_us;
  long deadline_us;
  long priority;
};



struct pruda_task_t {

  int id;

  struct gpu_sched_param gpu_params;
  int bs;
  int gs;

  struct sched_param param;

  pthread_t th;
  pthread_attr_t attr;

  sem_t wait_exec;

  cudaStream_t *str; 

  struct pruda_task_t * next;
  
};


#endif
