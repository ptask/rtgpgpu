#include "../inc/tools.h"



#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>



// // Kernel function to add the elements of two arrays
// __global__ void add()
// {
//   __allocate_to_sm(0);
//   printf("I am processed by %lu\n",(unsigned long)__get_smid());
// }




__global__ void kernel_a_code(){
    printf(" ******** this is the kernel a  code \n");
    int res = 0; 
    for (int i=0;i<500000000;i++){
      res +=i * 5;
    }
}



__global__  void kernel_b_code(){
printf(" ****************************** this is the kernel b code \n");
    int res = 0; 
    for (int i=0;i<500000000;i++){
      res +=i * 5;
    }
}

__global__  void kernel_c_code(){
  printf(" ***************************************** this is the kernel c code \n");
    int res = 0; 
    for (int i=0;i<500000000;i++){
      res +=i * 5;
    }
}



int main(int argc, char  ** argv){



  struct pruda_task_t * p_task_a = create_pruda_task(0, kernel_a_code, 1000000, 1000000, 15, 1, 1);
  struct pruda_task_t * p_task_b = create_pruda_task(1, kernel_b_code, 2500000, 2500000 , 20, 1, 1);

  init_scheduler(MULTIPLE, FP);
  add_pruda_task(p_task_a);
  add_pruda_task(p_task_b);
  
  printf("Sched initialed, creating cpu threads \n");

  create_cpu_threads();

  sleep(20);
}
