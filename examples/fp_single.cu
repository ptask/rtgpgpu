#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>



#include "../inc/user.h"
#include "../inc/tools.h"
#define N 8


template<typename ...Arguments>
void create_kernel(struct kernel_t<Arguments ...> * k, void kernel_c(Arguments...),  int gs, int bs,
						Arguments...args){

  k->kernel_c = kernel_c;
  k->args = std::tuple<Arguments...>(args...);
  k->gs = gs;
  k->bs = bs;
  
}



__global__ void add( int *a, int *b, int *c ) {

  printf("here 2 \n");
    int tid = blockDim.x * blockIdx.x + threadIdx.x;
    while (tid < N) {
        c[tid] = a[tid] + b[tid];      
        tid += blockDim.x;                                      
    }
}




__global__ void mul( int *a, int *b, int *c, int h ) {

  printf("here mul \n");
    int tid = blockDim.x * blockIdx.x + threadIdx.x;
    while (tid < N) {
        c[tid] = a[tid] * b[tid];      
        tid += blockDim.x;                                      
    }
}






int main(int argc, char  ** argv){
  
  int *a, *b, *c;  
  int *dev_a, *dev_b, *dev_c; 
  
  a = (int*)malloc( N * sizeof(int) );
  b = (int*)malloc( N * sizeof(int) );
  c = (int*)malloc( N * sizeof(int) );
  
  for (int i=0; i<N; i++) {
    a[i] = i;
    b[i] = i;
  }
  
  cudaMalloc( (void**)&dev_a, N * sizeof(int) );
  cudaMalloc( (void**)&dev_b, N * sizeof(int) );
  cudaMalloc( (void**)&dev_c, N * sizeof(int) );

   
  cudaMemcpy( dev_a, a, N * sizeof(int),cudaMemcpyHostToDevice );
  cudaMemcpy( dev_b, b, N * sizeof(int),cudaMemcpyHostToDevice );
     
  int ac=5;
  init_kernel_listing();
  create_kernel(std::get<1>(get_listing()),add,2,5,dev_a,dev_b,dev_c);
  create_kernel(std::get<0>(get_listing()),mul,2,5,dev_a,dev_b,dev_c,ac);


  struct gpu_sched_param gb;
  gb.period_us =  3000000;
  gb.deadline_us= 3000000;
  gb.priority = 20;
  

  struct gpu_sched_param ga;
  ga.period_us =  6000000;
  ga.deadline_us= 6000000;
  ga.priority = 15;



  struct pruda_task_t * p_task_b = create_pruda_task(1, gb, 1, 1);
  struct pruda_task_t * p_task_a = create_pruda_task(0, ga, 1, 1);

  init_scheduler(SINGLE, FP);
  add_pruda_task(p_task_a);
  add_pruda_task(p_task_b);

  

  create_cpu_threads();
    
  sleep(9);


  
  cudaMemcpy(c, dev_c, N * sizeof(int), cudaMemcpyDeviceToHost );



   
  free( a );
  free( b );
  free( c );
   
  cudaFree( dev_a );
  cudaFree( dev_b );
  cudaFree( dev_c );
     
  return 0;
}



 
  

//   struct gpu_sched_param c;
//   c.period_us =  30000;
//   c.deadline_us= 30000;
//   c.priority = 2;


//   struct pruda_task_t * p_task_b = create_pruda_task(1, kernel_b_code, b, 1, 1);
//   struct pruda_task_t * p_task_a = create_pruda_task(0, kernel_a_code, a, 1, 1);
//   struct pruda_task_t * p_task_c = create_pruda_task(2, kernel_c_code, c, 1, 1);


//   init_scheduler(SINGLE, FP);
//   add_pruda_task(p_task_a);
//   add_pruda_task(p_task_b);
//   add_pruda_task(p_task_c);


//   //  gpu_call_params(p_task_a,params);
  
//   printf("Sched initialed, creating cpu threads \n");

//   create_cpu_threads();

//   sleep(5);
 
// }
