void sporadicTask(void *arg) {
  <initialization>;
  
  addkernel(kernelA, gridSize, blockSize, param, priority, CHOSENSM, SMid);
  
  while(cond){
    
    launchOnlyChosenSm();
    
    <wait event>;
  }
  
  destroyOnlyChosenSm();
}