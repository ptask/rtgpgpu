\section{GPU programming and PRUDA primitives  }
\label{sec:gpu}

A GPU is compound of one or more \emph{streaming multiprocessors} (SMs)
and one or more \emph{copy engines} (CEs). Streaming multiprocessors are
able to achieve computations (\emph{kernels}), whereas copy engines execute
memory copy operations between different memory spaces. Programming
the GPU requires dividing parallel computations into several grids,
and each grid to several blocks. A block is a set of multiple
threads. A GPU can be programmed using generic platforms such OpenCL
or proprietary independent APIs. We use CUDA, a NVIDIA proprietary
platform, to have a tight control on SMs and CEs in the {\sf C}
programming language and using the NVIDIA compiler.

\begin{figure}[t]
    \centering

\resizebox{200px}{!}{
\newcommand\squaring[4]{
\draw[rounded corners](0+#1,0+#2)rectangle(3+#1,2+#2+#3);
\node at(#1+1.5,#2+1+#3/2){#4};
}
\begin{tikzpicture}


\squaring{0}{0}{0}{SM0 (128 cores)};
\squaring{3.2}{0}{0}{SM1 (128 cores)};
\squaring{1.8}{-1.5}{-1}{Cpy. Engine};



\squaring{-4.5}{0}{0}{Denver CPUs)};
\squaring{-4.5}{-2.2}{0}{A57 CPUs};

\draw[](-0.3,-3) rectangle (6.4,2.2);
\node at(3.5,-2.5){NVIDIA PASCAL GPU };

\draw[](-0.3-5,-3) rectangle (6.4-7,2.2);
\node at(3.5-6.5,-2.5){CPU  Islands};

\draw[](-5.8,-4) rectangle (7,-3.2);
\node at (0.6,-3.6){Shared Main Memory  };

\end{tikzpicture}
}
    \caption{Jetson TX2 Architecture}
    \label{fig:jetson}
\end{figure}

When a kernel is invoked by CPU code, it submits commands to the
GPU. How and when commands are executed, is hidden by constructors for
intellectual property concerns. Authors in \cite{amert2017gpu} have
tried to reveal some \emph{GPU scheduling secrets} by benchmarking a Jetson
TX2 (abbreviated TX2 in the rest of this paper). It is compound of 6
ARM-based CPU cores, along with an integrated NVIDIA PASCAL-based GPU
as shown in Figure \ref{fig:jetson}, all running onto Ubuntu. The
GPU in the TX2 is compound of 256 Cuda cores, divided into two SMs and
one copy engine. CPUs and GPU share the same memory module. From a
programming perspective, one may either allocate two separate memory
spaces for CPU and GPU using {\sf malloc} and {\sf CudaMalloc}
primitives respectively. The programmer may use a memory space visible
logically by the CPU and the GPU called CUDA unified memory (even for
discrete GPUs), therefore no memory copies are needed between CPU and
GPU tasks such memory spaces (buffers) allocated using the {\sf
  CudaMallocManaged} primitive. The current version of 
PRUDA supports CUDA unified memory to avoid dealing with memory copy
operations, as it will be shown in PRUDA architecture. An extension to
separate memory spaces is under development and will be soon
available.

Typical Cuda programs are organized in the same way. First, memory
is allocated both on CPU and GPU. Further,
memory copies are operated between CPU and GPU. Then, the GPU kernel
is launched, and finally results are copied back to the CPU by memory
copy operations.

Regarding kernel execution within the GPU, authors in
\cite{amert2017gpu} affirm that all threads of any block are executed
by only one SM, however different blocks of the same kernel may
be executed on different SMs. In Figure \ref{fig:sched_jetson}, the
green kernel is executed on both SM0 and SM1, the red SM is executed
only on SM0. The kernel execution order and mechanisms are driven by
internal closed-source NVIDIA drivers (in our case of study). A PRUDA
user may obtain the SM where a given block/thread is executing by using the {\sf
  pruda\_get\_sm()} function. PRUDA allows also enforcing the
allocation of a given kernel to a specific SM by using PRUDA function
{\sf pruda\_allocate\_to\_sm(int sm\_id)}, where the {\sf sm\_id} is
the id of the target streaming multiprocessor. Implementation details
about how these functions work can be found in the PRUDA description
section.


\begin{figure}[b]
    \centering
\resizebox{0.6\columnwidth}{!}{
\begin{tikzpicture}

\draw (0,0)--(5,0);
\draw (0,2)--(5,2);
\draw (0,1)--(5,1);

\draw [black, fill=green](0,0)rectangle (1,0.3);


\node at (-0.5,0.5){\scriptsize SM0};
\node at (-0.5,1.5){\scriptsize SM1};

\filldraw [color=black, fill=green!20](0,0)rectangle (1,0.3);
\filldraw [color=black, fill=green!20](0,1)rectangle (1,1.3);


\filldraw [color=black, fill=green!20](2.8,3)rectangle (3,3.2);
\filldraw [color=black, fill=red!20](2.8,2.7)rectangle (3,2.9);

\node at(3.7,3.1){\tiny Kernel 1};
\node at(3.7,2.8){\tiny Kernel 2};


\filldraw [color=black, fill=red!20](2,0)rectangle (4,0.3);

\end{tikzpicture}
}
    \caption{Example of Kernel scheduling in GPU}
    \label{fig:sched_jetson}
  \end{figure}
  
  To enforce an execution order between different kernels, we use a
  specific data structure, called \emph{Cuda Stream}. A cuda stream has a
  FIFO behavior. Therefore, kernels submitted to a Cuda stream are
  executed one after the other in a {\bf sequential}
  fashion. Therefore, synchronization between two consecutive kernels
  is implicitly achieved. This property will be used later to
  implement non preemptive EDF and fixed priority real-time scheduling
  policies.

  In Cuda, the user may define several streams. A priority might be
  set between different streams. Therefore, if a stream {\sf A} has a
  higher priority than stream {\sf B}, all kernels of {\sf A} are
  meant to execute before kernels that are submitted to {\sf B}. If a
  kernel in {\sf B} is executing, and a kernel is activated on
  {\sf A}, the GPU might preempt the kernel of {\sf B}, to execute the
  kernel of {\sf A} according to the GPU
  preemption level (we will show this behaviour in our benchmarks). 
  We highlight that fine-grain preemption
  capabilities are available in NVIDIA GPUs starting from the PASCAL
  architecture. For example, if a preemption is set at a block level,
  preemption will be achieved when all already executing blocks finish
  their execution. Recent VOLTA GPUs allow even finer preemption
  levels. 
  
  Even if it is possible to create more than 2 streams, only
  two levels of priority are available in the Jetson TX2 platform.
  These properties will be used later to approximate EDF and fixed
  priority preemptive scheduling policies.

Other PRUDA functions will be detailed later.
 


\section{System model}
\label{sec:models}
 
In this paper, we are only interested in GPU programming and
scheduling. While this paper provides real-time support to GPUs, 
we do not provide any schedulability analysis yet, the analysis is work in progress. 

We assume that all tasks in the system are programmed used PRUDA, therefore only PRUDA tasks
are in concurrence in GPU. Each task $\tau_i$ is characterized by its
deadline $\mathsf{D}_i$ and its period $\mathsf{T}_i$. Tasks are
strictly periodic, therefore the exact time between two successive
activations of task $\tau_{i}$ is equal to $\mathsf{T}_i$. The
$j^{th}$ instance of task $\tau_i$ must finish it execution no later
than $\mathsf{T}_i \times j + \mathsf{D}_i$, otherwise it {\it
  misses} its deadline. The task may be scheduled using fixed
priority, therefore it may be characterized by parameter priority
$\mathsf{P}_i$. From the implementation perspective, each PRUDA tasks is a
instance of a periodic CPU thread as shown in the algorithm of Figure
\ref{algo:pruda}.
 
 \begin{figure}
  \centering
  \begin{minted}[gobble=4,frame=lines]{c}
    void *pruda_task(void * arg) { 
      struct timespec_t next; 
      p_kernel_t *pk = (p_kernel_t *)(arg); 
      while (1) {
        // memory copy operation
        clock_gettime(CLOCK_REALTIME, &next);

        pruda_subscribe(pk->kernel,p->priority)
        
        timespec_addto(next, pk->T);
        clock_nanosleep(CLOCK_REALTIME, 0, &next, 0); 
      } 
    }
  \end{minted}
  
  \caption{Pseudo-code of PRUDA task}
\label{algo:pruda}
\end{figure}

The PRUDA task starts by parsing the kernel parameters which are the
kernel code, priority, deadline and period. Further it starts the
periodic task behavior. The task get the current time and computes the
next instance activation time {\sf next}. Later, the GPU job is
registered in the correct (according to the desired scheduling policy)
GPU run-queue (see PRUDA architecture in Figure
\ref{fig:pruda_show}). Once the PRUDA CPU thread launched the
kernel, it sleeps until the next activation. Another scheduling entity
checks the run-queue state and schedule the highest priority tasks
first according to (i) one of the strategies details into the next
section and (ii) to the desired scheduling policy.

The memory copy operation line achieves memory copies. This operation
may need to copy several buffers from CPU to GPU and vice-versa. The
current version of the platform use Cuda unified memory, therefore
memory coherency is achieved automatically by the NVIDIA Driver.

The GPU may be scheduled as a single core platform or a parallel
platform where each streaming multiprocessor is an independent core by
the mean of the PRUDA ${\sf \_\_allocate\_to\_sm(\cdots)}$
function. The allocation to a given SM is achieved by testing if the
task is in the correct SM, if yes, the computation is achieved,
otherwise the thread on the {\it wrong} SM is killed.





\begin{figure*}[ht]
    \centering
\newcommand\unefile[3]{
\foreach \x in {1,1.5,...,#3}
{
\draw (#1+\x,#2) rectangle (#1+0.5+\x,#2+0.5);
}
}

\newcommand\arrowtext[5]{
\draw [->,line width=0.5mm, #5](#1,#2)--(#1+#3,#2);
\node at (#1+#3/2,#2+0.2){#4};
}
\resizebox{0.8\textwidth}{!}{
\begin{tikzpicture}
\unefile{0}{0}{3};

\edef\mya{7}

\foreach \z in {2,1.25,...,-2}
   {

  \unefile{5}{\z}{4};

 \pgfmathparse{int(\mya-1)}
 \xdef\mya{\pgfmathresult}
 \node at(9.9,\z+0.2){P$_{\mya}$};

}

\draw (5.8,2.75) rectangle (10.5,-3.5);
\node[rotate=90] at (8,-2.5){\Huge ...};




\arrowtext{-2}{0.25}{3}{\sf pruda\_add\_task}{red}
\node at (2.5,-0.25){task queue {\sf (tq)}};


\node at (8.25,-3.25){Active task queue {\sf (rq)}};


\foreach \xs in {2.5,1.75,...,-1.5}
\draw [->,dashed] (3.5,0.25)--(6,\xs-0.25);


\draw [dashed](5,-5.8) circle (1.5);
\draw [fill=white, color=white](3.5,-4.85) rectangle (7,-4);
\draw [->](3.73,-5)--(3.89,-4.77);

\draw [color=red, line width=0.5mm, ->] (5,-.75)--(5,-4.25);

\node at (5.25,-4.5){{\sf pruda\_subscribe}};






\unefile{14}{0.5}{4};
\unefile{14}{-0.5}{4};
\draw (11.5+3,1.5) rectangle (17+3,-1.5);
\node at (17.25,-1.25){{\sf stream queues }};
\node at (19.25,0.75){{\sf h-sq }};
\node at (19.25,-.25){{\sf l-sq }};





\arrowtext{10.5}{0.25}{4}{\sf pruda\_resched}{red}




\def\xx{-3.5}
\def\yy{-4.5}

\unefile{14+\xx}{0.5+\yy}{3};
\unefile{14+\xx}{-0.5+\yy}{3};
\node at (18.25+\xx,0.7+\yy5){{\sf SM0-q }};
\node at (18.25+\xx,-.25+\yy){{\sf SM1-q }};



\draw [color=red, line width=0.5mm,->](17-6.5,0)--(11,0)--(11,-4.25)--(11.5,-4.25);
\node[rotate=90] at (11.25,-2){pruda\_alloc};



\draw [color=red, line width=0.5mm,->](14,-4.25)--(14.25,-4.25)--(14.25,0)--(14.5,0);
\node[rotate=90] at (14.,-2){pruda\_resched};






\def\xxx{0.75}
\draw (22+\xxx,3.5) rectangle (30+\xxx,-3.5);
\draw (23+\xxx,2.5) rectangle (25.5+\xxx,-1);
\draw (26+\xxx,2.5) rectangle (28.5+\xxx,-1);

\arrowtext{20}{0.25}{2.75}{\sf GPU internals}{red}

\node at (25,-0.75){SM0};
\node at (28,-0.75){SM1};


\node at (26.5,-1.75){\sf pruda\_check};
\node at (26.5,-2.25){\sf pruda\_abort};



\end{tikzpicture}
}
    \caption{PRUDA global overview}
    \label{fig:pruda_show}
\end{figure*}
