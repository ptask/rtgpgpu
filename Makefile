CC      =  nvcc

main_1: user.o runqueue.o tools.o timeops.o fp_single.o user.o
	$(CC)   user.o tools.o runqueue.o timeops.o test.o -o  testing  


fp_single.o : examples/fp_single.cu
	$(CC)  -c examples/fp_single.cu -o test.o -dc


user.o : src/user.cu
	$(CC) -c src/user.cu -o user.o  

timeops.o : src/timeops.cu
	$(CC) -c src/timeops.cu -o timeops.o -dc

runqueue.o : src/runqueue.cu
	$(CC)  -c src/runqueue.cu -o runqueue.o -dc


tools.o : src/tools.cu 
	$(CC) -c src/tools.cu -o tools.o -dc 



clean:
	rm -f *.o testing *~
