#include "../inc/runqueue.h"



struct pruda_runqueue_t * create_pruda_runqueue(){
  struct pruda_runqueue_t * rq = (struct pruda_runqueue_t *)(malloc(sizeof(struct pruda_runqueue_t)));
  rq->size = 0;
  return rq;
}

int add_tail_pruda_task_runqueue(struct pruda_task_t * task, struct pruda_runqueue_t * rq){
  if (rq == NULL){
    printf("task queue is empty, exiting \n");
    exit(-1);
  }
  if (rq->size >= MAX_PRUDA_TASK_PER_QUEUE){
    fprintf( stderr, "Runqueue size exceeded, pruda task adding failed ");
    return FAIL;
  }
  rq->list[rq->size] = task;
  rq->size++;
  return SUCCESS;
}



int del_tail_pruda_task_runqueue(struct pruda_runqueue_t *rq){
  if (rq->size <= 0){
    fprintf( stderr, "Runqueue  is empty, nothing to delete");
    return FAIL;
  }
  rq->size--;
  return SUCCESS;
}


struct pruda_task_t  * get_tail_pruda_task_runqueue(struct pruda_runqueue_t *rq){
  return rq->list[rq->size-1];
}

void destroy_pruda_runqueue(struct pruda_runqueue_t *rq){
  free(rq);
}

// runqueue list operations
struct pruda_runqueue_list_t * create_pruda_runqueues_list(){
  struct pruda_runqueue_list_t *rql =  (struct pruda_runqueue_list_t *)(malloc(sizeof(struct pruda_runqueue_list_t)));
  for (int i=0;i< RUNQUEUES_NMB;i++)
    rql->list[i]=create_pruda_runqueue();

  return rql;
}

struct pruda_runqueue_t * get_most_priority_queue_fixed_priority(struct pruda_runqueue_list_t *rql){
   for (int i=0;i< RUNQUEUES_NMB;i++)
     if (rql->list[i]->size >0)
       return rql->list[i];
  return NULL;
}


void print_rq_state(struct pruda_runqueue_list_t *rql){
  printf("15  %d |  20: %d |  2: %d \n", rql->list[15]->size, rql->list[20]->size,
	 rql->list[2]->size); 
}

void destroy_pruda_runqueue_list_t(struct  pruda_runqueue_list_t * rql){
  for (int i=0;i<RUNQUEUES_NMB;i++)
    destroy_pruda_runqueue(rql->list[i]);
}


void add_pruda_task_fixed_priority(struct pruda_task_t * tau, struct pruda_runqueue_list_t * rql){
  add_tail_pruda_task_runqueue(tau,rql->list[tau->gpu_params.priority]);
}


struct pruda_task_t *  get_most_priority_task_fixed_priority(struct pruda_runqueue_list_t * rql){
  struct pruda_runqueue_t * rq = get_most_priority_queue_fixed_priority(rql);
  if (rq!=NULL)
    return rq->list[rq->size-1];
  return NULL;
  
}
