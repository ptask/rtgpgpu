#include "../inc/timeops.h"

int cmp_spec(struct timespec *a, struct timespec *b){
return 0;

}

void  add_spec_us(struct timespec * s, long time_us){
  s->tv_nsec += time_us * 1000;
  while (s->tv_nsec >= 1000000000) {
    s->tv_nsec = s->tv_nsec - 1000000000;
    s->tv_sec += 1;
  }
}
