#include "../inc/tools.h"
#include "../inc/user.h"

/* Assigns parameters of a kernel. 
 @ k the kernel struct which the user want to fille
 @ kernel_c  pointer to the source code of the kernel function (must be global)
 @ gs the grid size of kernel_c
 @ bs the bloc size of kernel_c 
 @ args ... The list of arguments of kernel_c   
*/

template<typename ...Arguments>
void create_kernel(struct kernel_t<Arguments ...> * k, void kernel_c(Arguments...),  int gs, int bs,
						Arguments...args){
  k->kernel_c = kernel_c;
  k->args = std::tuple<Arguments...>(args...);
  k->gs = gs;
  k->bs = bs;
  
}


// A set of template to apply tuples as kernel parameters.
template<int...> struct index_tuple{}; 

template<int I, typename IndexTuple, typename... Types> 
struct make_indexes_impl; 

template<int I, int... Indexes, typename T, typename ... Types> 
struct make_indexes_impl<I, index_tuple<Indexes...>, T, Types...> 
{ 
    typedef typename make_indexes_impl<I + 1, index_tuple<Indexes..., I>, Types...>::type type; 
}; 

template<int I, int... Indexes> 
struct make_indexes_impl<I, index_tuple<Indexes...> > 
{ 
    typedef index_tuple<Indexes...> type; 
}; 

template<typename ... Types> 
struct make_indexes : make_indexes_impl<0, index_tuple<>, Types...> 
{}; 

template<class Ret, class... Args, int... Indexes > 
Ret apply_helper(int gs, int bs, Ret (*pf)(Args...), index_tuple< Indexes... >, std::tuple<Args...>&& tup) 
{ 
  (*pf)<<<gs,bs>>>( std::forward<Args>( std::get<Indexes>(tup))... );
} 

template<class Ret, class ... Args> 
Ret apply(int gs, int bs, Ret (*pf)(Args...), const std::tuple<Args...>&  tup)
{
  return apply_helper(gs, bs, pf, typename make_indexes<Args...>::type(), std::tuple<Args...>(tup));
}








static __device__ __inline__ uint32_t __get_smid(){    
  uint32_t smid;    
  asm volatile("mov.u32 %0, %%smid;" : "=r"(smid));    
  return smid;
  }

static __device__ __inline__ int  __check_to_sm(uint32_t sm){     
  return __get_smid()==sm;
  }

static __device__ __inline__ void  __allocate_to_sm(int sm){    
       if (!__check_to_sm(sm))
       	  asm("exit;");
  }



static __device__ __inline__ void  __allocate_to_sm(int cond){    
       if (cond)
       	  asm("trap;");


  }




struct scheduler_t  * scheduler; 


// Houssam : need to declare indexes methods 
void pruda_alloc_sm(int sm){
}

int  pruda_get_sm(){
  return 0;
}
int  pruda_check_sm(int sm){
  return 0;
}

void pruda_thread_exit(){
}
void pruda_kernel_abort(){
}




void init_scheduler(int strategy, int policy){

  
  scheduler = (struct scheduler_t *) (malloc(sizeof(struct scheduler_t)));

  scheduler->strategy = strategy;
  scheduler->policy = policy;

  scheduler->tq  = create_pruda_runqueue();
  scheduler->rql = create_pruda_runqueues_list();


  // Houssam: This part need to be redefined so to dynamically be parametrized  
  scheduler->sm0rq= create_pruda_runqueue();
  scheduler->sm1rq=create_pruda_runqueue();


 scheduler->mut = PTHREAD_MUTEX_INITIALIZER;
  switch (policy)
    {
    case EDF:	
      scheduler->pruda_subscribe = &pruda_subscribe_edf;
      scheduler->pruda_resched = &pruda_resched_edf;
      break;
      
    case FP:
      scheduler->pruda_subscribe = &pruda_subscribe_fp;
      scheduler->pruda_resched = &pruda_resched_fp;
      break;
      
    default:
      printf("Unknown scheduling policy, exiting ...  \n");
      exit(-1);
    }
   


  int lp,hp ;
  cudaDeviceGetStreamPriorityRange(&lp,&hp); 

  
  cudaStreamCreateWithPriority(&(scheduler->hsq), cudaStreamNonBlocking,hp);
  cudaStreamCreateWithPriority(&(scheduler->lsq), cudaStreamNonBlocking,lp);

  

  scheduler->lsq_free = 0;
  scheduler->hsq_free = 0;

}


void pruda_subscribe_fp(struct pruda_task_t *tau){

  // pthread_mutex_lock(&(scheduler->mut));
  if (tau->gpu_params.priority < 0 || tau->gpu_params.priority >= RUNQUEUES_NMB ){
    printf("Task priority out of Range, exiting \n");
    exit(-1);
  }
  add_pruda_task_fixed_priority(tau,scheduler->rql);
  // pthread_mutex_unlock(&(scheduler->mut));
  scheduler->pruda_resched();
}


void submit_task(int indexex){

 
  auto tasks = get_listing();
    switch ( indexex )
    {
    case 0:
       apply(std::get<0>(tasks)->gs,std::get<0>(tasks)->bs,
	     std::get<0>(tasks)->kernel_c,std::get<0>(tasks)->args);
	 
      break;

     case 1:
        apply(std::get<1>(tasks)->gs,std::get<1>(tasks)->bs,std::get<1>(tasks)->kernel_c,std::get<1>(tasks)->args);
       break;

    // case 2:
    //   apply(get<2>(tasks)->gs,get<2>(tasks)->bs,get<2>(tasks)->kernel_c,get<2>(tasks)->args);
    //   break;
	   
	   
    default:
      printf("unknown task, exitting \n");
      exit(-1);
    }
}


void sched_on_lsq(){

  struct pruda_runqueue_t * rq_h = get_most_priority_queue_fixed_priority(scheduler->rql);
 
  if (rq_h != NULL) {
    
    struct pruda_task_t * mp  = rq_h->list[rq_h->size-1];
    del_tail_pruda_task_runqueue(rq_h);

    mp->str = &(scheduler->lsq);
    
    //    (*(mp->kernel_func))<<<mp->gs,mp->bs,0,(*(mp->str)) >>>();
    submit_task(mp->id);
    cudaError_t code2= cudaGetLastError(); 
    if (code2 != cudaSuccess) 
      {
	printf("Running error: %s \n", cudaGetErrorString(code2));
	exit(-1);
      }
    sem_post(&(mp->wait_exec));
    scheduler->lsq_free =1;
    scheduler->lsq_current = mp;
  }

}



// Houssam : May be I will merge it with sched_on_lsq
void sched_on_hsq(){
  struct pruda_runqueue_t * rq_h = get_most_priority_queue_fixed_priority(scheduler->rql);
  if (rq_h != NULL) {
    struct pruda_task_t * mp  = rq_h->list[rq_h->size-1];
    del_tail_pruda_task_runqueue(rq_h);
    
    mp->str = &(scheduler->hsq);
    
    //    (*(mp->kernel_func))<<<mp->gs,mp->bs,0,(*(mp->str)) >>>();

    submit_task(mp->id);
    
    cudaError_t code2= cudaGetLastError(); 
    if (code2 != cudaSuccess) 
      {
	printf("Running error: %s \n", cudaGetErrorString(code2));
	exit(-1);
      }

    printf("task is %d for hsq  \n", mp->id);
    sem_post(&(mp->wait_exec));
    scheduler->hsq_free =1;
    scheduler->hsq_current = mp;
  }
}

void pruda_resched_fp_single(){
  pthread_mutex_lock(&(scheduler->mut));
  if (scheduler->lsq_free == 1) {
     pthread_mutex_unlock(&(scheduler->mut));
    return;
  }
  sched_on_lsq();
  pthread_mutex_unlock(&(scheduler->mut));
}

void pruda_resched_fp_multiple(){
  pthread_mutex_lock(&(scheduler->mut));
  printf("resched multiple called \n");
  if (scheduler->hsq_free == 1)
    {
      pthread_mutex_unlock(&(scheduler->mut));
      return;

    }
  printf("hi not occupied \n");
  if (scheduler->lsq_free==0)
    {
      printf("calling to schedule on lsq \n");
      sched_on_lsq();
      printf("scheduled on lsq \n");
    }
  else
    {
      printf("calling to schedule on hsq \n");
      struct pruda_runqueue_t * rq_h = get_most_priority_queue_fixed_priority(scheduler->rql);
      if (rq_h == NULL)
	{
	  pthread_mutex_unlock(&(scheduler->mut));
	  return;
	}
      struct pruda_task_t * mp  = rq_h->list[rq_h->size-1];
      if (mp->gpu_params.priority<scheduler->lsq_current->gpu_params.priority)
	{
	  printf("passed hsq tests \n");
	  sched_on_hsq();
	  printf("scheduled on hsq \n");
	}
      else {
	printf("hsq testes not passed \n");
      }
    }
  pthread_mutex_unlock(&(scheduler->mut));
}

void pruda_resched_multiproc(){}

void pruda_resched_fp(){  
  switch (scheduler->strategy)
    {
    case SINGLE:	
      pruda_resched_fp_single();
      break;
    case MULTIPLE:
      pruda_resched_fp_multiple();
      break;
    case MULTIPROC:
      pruda_resched_multiproc();
      break;
    default:
      printf("Unknown strategy, exiting ...  \n");
      exit(-1);
    }
}

void pruda_subscribe_edf(struct pruda_task_t *tau){

  // Houssam : need to be completed init params 
}

void pruda_resched_edf(){
  // Houssam : need to be completed init params 
}


void reset_pruda_task_queue(){
  // Houssam : need to be completed init params 
  //return reset_pruda_runqueue(scheduler->tq);
}

int add_pruda_task(struct pruda_task_t *tau){
  return add_tail_pruda_task_runqueue(tau, scheduler->tq);
}

int del_tail_pruda_task_from_tq(){
  return del_tail_pruda_task_runqueue(scheduler->tq); 
}

struct pruda_task_t * create_pruda_task(int id,
					struct gpu_sched_param gpu_params,
					int bs, int gs){

  
  struct pruda_task_t * task = (struct pruda_task_t *)(malloc(sizeof(struct pruda_task_t)));
  
  task->id = id;
  task->gpu_params.period_us=gpu_params.period_us;
  task->gpu_params.deadline_us=gpu_params.deadline_us;
  task->gpu_params.priority=gpu_params.priority;
  task->bs=bs;
  task->gs=gs;
  task->str = NULL;
  sem_init(&(task->wait_exec), 0, 0); 

  return task; 
}

void print_pruda_task(const struct pruda_task_t *task){
  if (!task){
    printf("Task is an empty pointer \n");
    return;
  }
  printf("[Task: id= %d, T= %lu , D= %lu, P= %lu, gs= %d, bs= %d ] \n",
	 task->id, task->gpu_params.period_us, task->gpu_params.deadline_us, task->gpu_params.priority,
	 task->gs, task->bs);
}




void *pruda_task(void *args){

  struct pruda_task_t *task = (struct pruda_task_t *)(args);
  struct timespec next;



  pthread_barrier_wait(&(scheduler->sync_all_barrier));

   while (1){

    
    clock_gettime(CLOCK_REALTIME, &next);
    add_spec_us(&next,task->gpu_params.period_us);

    scheduler->pruda_subscribe(task);

    // Sync task execution 
    sem_wait(&(task->wait_exec));
    // check if the next task is active, if yes !! resched it without waiting 
    cudaError_t code= cudaStreamSynchronize(*(task->str));
    if (code != cudaSuccess) 
      {
	printf("Stream synchronization error : %s \n", cudaGetErrorString(code));
	exit(-1);
      }	
    if (task->str == &(scheduler->hsq)){
	scheduler->hsq_free = 0;
      }
    else {
      scheduler->lsq_free = 0;
    }
    scheduler->pruda_resched();

    
    clock_nanosleep(CLOCK_REALTIME,TIMER_ABSTIME,&next,NULL);
    
  }
}



void create_cpu_threads(){

  pthread_barrier_init(&(scheduler->sync_all_barrier),NULL, scheduler->tq->size);
  for (int i=0;i<scheduler->tq->size;i++){

  
  pthread_attr_init(&(scheduler->tq->list[i]->attr));
  pthread_attr_setschedpolicy(&(scheduler->tq->list[i]->attr), SCHED_FIFO);
  scheduler->tq->list[i]->param.sched_priority =  scheduler->tq->list[i]->gpu_params.priority;
  pthread_attr_setschedparam(&(scheduler->tq->list[i]->attr), &(scheduler->tq->list[i]->param));   

   
  pthread_create(&(scheduler->tq->list[i]->th), &(scheduler->tq->list[i]->attr), pruda_task, scheduler->tq->list[i]);
  }
}

int pruda_task_queue_size(){
  return scheduler->tq->size;
}
